This servlet tracks changes in the Content Types and their 10 oldest samples in dotCMS.


# Installation
This plugin must be built from source and deployed to dotCMS' OSGi framework.

## Dependencies
This plugin requires the StructuresSnapshotService provided by the package [edu.msu.anr.osgi.structuralintegrity.service](https://gitlab.msu.edu/canr/edu.msu.anr.osgi.structuralintegrity.service) to be installed in order to work properly.

## Configuring
This plugin can be configured using the properties in the file 'src/main/resources/bundle.properties'. Valid properties are as follows:

* allowedIpAddresses - The comma-separated list of IP addresses which are allowed access to this servlet regardless of whether the user is logged in. This is intended to allow monitoring applications easy access. For example, `allowedIpAddresses = 127.0.0.1, 192.168.0.1, 35.9.20.200`. Default value is '127.0.0.1'.
* allowedRoleFqn - The fully-qualified name of the role which grants access to this servlet. For example, `allowedRoleFqn = Structural Integrity Checker`, or `allowedRoleFqn = Level One Role --> Level Two Role --> Level Three Role`. Default value is 'Structural Integrity Checker'.

## Building
In order to build this plugin, use the included Gradle wrapper to run the 'jar' task.

On Windows:
```
.\gradlew.bat jar
```

On Linux or MacOS:
```
./gradlew jar
```

The resulting .jar file should be located in './build/libs'.

## Deploying
In order to deploy the compiled plugin to dotCMS' OSGi framework, you may run the Gradle "load" task, upload the plugin .jar through dotCMS' Dynamic Plugins portlet, or manually place the plugin in the dotCMS instance's Apache Felix load directory.

### Load Task
This project's build.gradle file defines a task called "load" which builds this plugin, undeploys previous versions, and deploys the new version to the Apache Felix load directory. In order to run this task, you must set the environment variable "FELIXDIR" to the dotCMS instance's Apache Felix base directory. The load task is recommended for those with filesystem access to the dotCMS instance.

```
> FELIXDIR="$dotcms/dotcms_3.3.2/dotserver/tomcat-8.0.18/webapps/ROOT/WEB-INF/felix"
> export FELIXDIR
> ./gradlew load
```

### Filesystem
Apache Felix monitors its "load" and "undeploy" directories to install and uninstall plugin .jar files. You can move .jar files on the filesystem to load and unload them from the OSGi framework.
```
> mv "$dotcms/dotserver/tomcat-8.0.18/webapps/ROOT/WEB-INF/felix/load/edu.msu.anr.osgi.structuralintegrity.service-*.jar" "$dotcms/dotserver/tomcat-8.0.18/webapps/ROOT/WEB-INF/felix/undeploy"
> cp ./build/libs/*.jar "$dotcms/dotserver/tomcat-8.0.18/webapps/ROOT/WEB-INF/felix/load/"
```

# Usage
In order to use this servlet, visit the page '/app/integrity' on your site. The servlet will generate a snapshot of the system's Structures (Content Types) at the time of the request and compare that snapshot to the most recent saved snapshot. If the snapshots are the same, the servlet will output the heading "Same" and the time of the last change. If the snapshots differ, the servlet will output the heading "Different" and save a snapshot to a file in the "dotsecure/structuralIntegritySnapshots" directory.