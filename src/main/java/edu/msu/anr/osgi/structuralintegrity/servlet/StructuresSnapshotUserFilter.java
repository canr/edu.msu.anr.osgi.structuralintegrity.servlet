package edu.msu.anr.osgi.structuralintegrity.servlet;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dotmarketing.business.APILocator;
import com.dotmarketing.business.NoSuchUserException;
import com.dotmarketing.business.Role;
import com.dotmarketing.exception.DotDataException;
import com.dotmarketing.exception.DotSecurityException;
import com.dotmarketing.util.Logger;
import com.liferay.portal.model.User;

/**
 * A Java servlet filter to restrict access to authenticated CMS users in the given group or unauthenticated users from a given set of IP addresses.
 * @author slenkeri
 */
public class StructuresSnapshotUserFilter implements Filter {
	
	private static final String BUNDLE_PROPERTIES_FILE = "/bundle.properties";
	private static final String ALLOWED_ROLE_FQN_KEY = "allowedRoleFqn";
	private static final String ALLOWED_IP_ADDRESSES_KEY = "allowedIpAddresses";
	
	/** The fully-qualified name of the role which grants access to this servlet. */
	private String allowedRoleFqn = "Structural Integrity Checker";

	/** The list of IP addresses which are allowed access to this servlet regardless of whether the user is logged in. */
	private List<String> allowedIpAddresses = Arrays.asList("127.0.0.1");
	

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		Properties props = this.loadProperties();

		String allowedRoleFqnValue = props.getProperty(StructuresSnapshotUserFilter.ALLOWED_ROLE_FQN_KEY);
		if (allowedRoleFqnValue != null) {
			this.allowedRoleFqn = allowedRoleFqnValue;
		}
			
		String allowedIpAddressesValue = props.getProperty(StructuresSnapshotUserFilter.ALLOWED_IP_ADDRESSES_KEY);
		if (allowedIpAddressesValue != null) {
			this.allowedIpAddresses = Arrays.asList(allowedIpAddressesValue.split(",\\s*"));
		}
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
			throws IOException, ServletException {
		// Convert request and response to HTTP classes
		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) res;
		HttpSession session = request.getSession();
		
		try {
			if (isRequestFromAuthorizedIp(request) || isUserInAuthorizedRole(getSessionUser(session))) {
				chain.doFilter(request, response);
			}
		} catch (NoSuchUserException|DotSecurityException e) {
			response.sendError(401);
			return;
		} catch (DotDataException e) {
			response.sendError(500);
			return;
		}		
	}

	@Override
	public void destroy() {
		// Nothing to destroy
	}
	
	/**
	 * Checks whether the request originates from an authorized IP address.
	 * @param request The incoming HTTP request.
	 * @return Whether the request's IP is found in the list of authorized IP addresses loaded from the properties file.
	 */
	private boolean isRequestFromAuthorizedIp(HttpServletRequest request) {
		String clientIp = request.getHeader("X-FORWARDED-FOR") == null ?
				request.getRemoteAddr() :
				request.getHeader("X-FORWARDED-FOR");

		return this.allowedIpAddresses.contains(clientIp);
	}
	
	/**
	 * Gets the current session's logged in dotCMS User.
	 * @param session The current HTTP session.
	 * @return The current session's logged in user.
	 * @throws NoSuchUserException If the session's user does not exist.
	 * @throws DotDataException If there is an error loading the User from the database.
	 * @throws DotSecurityException If the User looking up the session's User does not have permission to the session User.
	 */
	private User getSessionUser(HttpSession session) throws DotSecurityException, DotDataException {
		Object userIdRaw = session.getAttribute("USER_ID");
		if (userIdRaw == null) {
			throw new NoSuchUserException("Not logged in.");
		}
		String userId = (String) userIdRaw;
		
		return APILocator.getUserAPI().loadUserById(userId);
	}
	
	/**
	 * Checks whether the given user has a role which authorizes them to complete the request.
	 * @param user The dotCMS User making the request.
	 * @return Whether the given user is in the role which is configured to be authorized.
	 * @throws DotDataException If the given user or configured role does not exist.
	 */
	private boolean isUserInAuthorizedRole(User user) throws DotDataException {
		Role role = APILocator.getRoleAPI().findRoleByFQN(this.allowedRoleFqn);
		return role != null && APILocator.getRoleAPI().doesUserHaveRole(user, role);
	}
	

    /**
     * Loads the bundle's properties from the packaged config file if it exists.
     * @return Properties object into which the bundle's properties file's contents are loaded.
     */
    private Properties loadProperties() {
		Properties props = new Properties();

		try {
			InputStream in = new BufferedInputStream(this.getClass().getClassLoader().getResourceAsStream(BUNDLE_PROPERTIES_FILE));
			props.load(in);
			in.close();
		} catch (Exception e) {
			Logger.error(this, e.getMessage());
		}

		return props;
    }
}