package edu.msu.anr.osgi.structuralintegrity.servlet;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import com.dotcms.repackage.org.osgi.util.tracker.ServiceTracker;
import com.dotmarketing.exception.DotDataException;
import com.dotmarketing.exception.DotSecurityException;
import com.dotmarketing.util.Logger;

import edu.msu.anr.osgi.structuralintegrity.service.StructuresSnapshot;
import edu.msu.anr.osgi.structuralintegrity.service.StructuresSnapshotService;


/**
 * A servlet that handles structural integrity requests
 * @author slenkeri
 */
public class StructuresSnapshotServlet extends HttpServlet {

    private static final long serialVersionUID = 42L;
    private final transient ServiceTracker<StructuresSnapshotService, StructuresSnapshotService> serviceTracker;
    
    /**
     * Constructs a Structures Snapshot servlet.
     * @param serviceTracker An OSGi service tracker which tracks StructuresSnapshotService requests.
     */
    public StructuresSnapshotServlet ( ServiceTracker<StructuresSnapshotService, StructuresSnapshotService> serviceTracker ) {
    	Logger.info(this, "Constructing StructuresSnapshotServlet.");
        this.serviceTracker = serviceTracker;
    }


    /**
     * Handles HTTP GET requests.
     * @param httpServletRequest The incoming HTTP request to this servlet.
     * @param httpServletResponse The HTTP response from this servlet.
     */
    @Override
    protected void doGet ( HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse ) throws ServletException, IOException {
        ServletOutputStream out = httpServletResponse.getOutputStream();
        httpServletResponse.setContentType( "text/html" );
        out.println( "<html><body>" );

        StructuresSnapshotService service = serviceTracker.getService();
        if ( service != null ) {

            try {
            	StructuresSnapshot currentSnapshot = service.takeSnapshot();

            	if (service.getSavedSnapshotFiles().length != 0) {
            		// Diff snapshots
					StructuresSnapshot previousSnapshot = service.getPreviousSnapshot();

					if (!currentSnapshot.isEquivalent(previousSnapshot)) {
						service.saveSnapshot(currentSnapshot);
						out.println("<h1>Different</h1>");
						out.println("<p>Snapshot differs from the snapshot taken on "
								+ previousSnapshot.timestamp.toLocalDate().toString() + " at "
								+ previousSnapshot.timestamp.toLocalTime().toString() + ".</p>");
						out.println("<p>Saving current snapshot.</p>");
					}
					else {
						out.println("<h1>Same</h1>");
						out.println("<p>Snapshots are identical.</p>");
						out.println("<p>Last change on "
								+ previousSnapshot.timestamp.toLocalDate().toString() + " at "
								+ previousSnapshot.timestamp.toLocalTime().toString() + ".");
					}
            	} else {
            		// Take 1st snapshot
            		service.saveSnapshot(currentSnapshot);
            		out.println("<h1>Activated</h1><p>Saved first snapshot.</p>");
            	}
            } catch (DotDataException|DotSecurityException e) {
            	Logger.error(this, "Unable to take snapshot.");
            	out.println("<p>Unable to take snapshot.</p>");
			} catch (IOException|ClassNotFoundException|IndexOutOfBoundsException e) {
				Logger.error(this, "Unable to load previous snapshot.");
				out.println("<p>Unable to load previous snapshot.</p>");
			} catch (Exception e) {
				Logger.error(this, e.getMessage());
			}

        } else {
        	Logger.error(this, "Unable to find StructuresSnapshotService.");
        }

        out.println( "</body></html>" );
        out.close();
    }

}