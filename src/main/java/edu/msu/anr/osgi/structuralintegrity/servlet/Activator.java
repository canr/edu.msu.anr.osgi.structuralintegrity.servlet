package edu.msu.anr.osgi.structuralintegrity.servlet;

import com.dotcms.repackage.org.apache.felix.http.api.ExtHttpService;
import com.dotcms.repackage.org.osgi.framework.BundleContext;
import com.dotcms.repackage.org.osgi.framework.ServiceReference;
import com.dotcms.repackage.org.osgi.util.tracker.ServiceTracker;
import com.dotmarketing.osgi.GenericBundleActivator;

import com.dotmarketing.util.Logger;
import edu.msu.anr.osgi.structuralintegrity.service.StructuresSnapshotService;

/**
 * OSGi Activator class provides methods for starting and stopping this bundle.
 * @author slenkeri
 */
public class Activator extends GenericBundleActivator {

    private StructuresSnapshotServlet snapshotServlet;
    private ExtHttpService httpService;
    private ServiceTracker<StructuresSnapshotService, StructuresSnapshotService> structuresSnapshotServiceTracker;
    private StructuresSnapshotUserFilter filter = new StructuresSnapshotUserFilter();


    /**
     * Implements BundleActivator.start(). Registers an
     * instance of a test service using the bundle context;
     * attaches properties to the service that can be queried
     * when performing a service look-up.
     *
     * @param context The framework context for the bundle.
     */
    @SuppressWarnings ("unchecked")
    @Override
    public void start ( BundleContext context ) throws Exception {

        //Initializing services...
        initializeServices( context );

        //Create new ServiceTracker for StructuralIntegrityService
        try {
            String className = StructuresSnapshotService.class.getName();
            structuresSnapshotServiceTracker = new ServiceTracker<>(
                    context,
                    className,
                    null);
        } catch (Exception e) {
            Logger.error(this, e.getMessage());
        }

        //Service reference to ExtHttpService that will allows to register servlets and filters
        ServiceReference<StructuresSnapshotService> sRef = (ServiceReference<StructuresSnapshotService>) context.getServiceReference( ExtHttpService.class.getName() );
        if ( sRef != null ) {

            structuresSnapshotServiceTracker.addingService( sRef );
            httpService = (ExtHttpService) context.getService( sRef );
            try {
                //Register a snapshot servlet
                snapshotServlet = new StructuresSnapshotServlet( structuresSnapshotServiceTracker );
                httpService.registerServlet( "/integrity", snapshotServlet, null, null );
                httpService.registerFilter(this.filter, "/integrity/.*", null, 100, null);
            } catch ( Exception e ) {
                e.printStackTrace();
            }
        }

        // open service tracker to start tracking
        structuresSnapshotServiceTracker.open();
    }

    /**
     * Implements BundleActivator.stop(). Un-registers the servlet and removes the filter.
     * @param context The framework context for the bundle.
     */
    @Override
    public void stop ( BundleContext context ) throws Exception {
        // Unregister the servlet
        if ( httpService != null && snapshotServlet != null ) {
            httpService.unregisterServlet( snapshotServlet );
            httpService.unregisterFilter(this.filter);
        }
        
        // Close service tracker to stop tracking
        structuresSnapshotServiceTracker.close();
    }

}